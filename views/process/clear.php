<?php

session_start();

include_once ("../../vendor/autoload.php");

use App\Database\Database;
use App\Utility\Utility;
use App\Teacher\Teacher;

$objDB = new Database();
$objTeacher = new Teacher();


if ($_GET['table'] == 'courses')
{
    $status = $objDB->deleteAll('assign_course');

    if ($status)
    {
        $objTeacher->resetAllTeacher();
        $_SESSION['message'] = "Data Deleted!!";
        header('location: ../clear.php?from=courses');
    }
    else
    {
        $_SESSION['message'] = "Error! Something Wrong!!";
        header('location: ../clear.php?status=error');
    }
}
elseif ($_GET['table'] == 'schedule')
{
    $status = $objDB->deleteAll('allocate_rooms');

    if ($status)
    {
        $_SESSION['message'] = "Data Deleted!!";
        header('location: ../clear.php?from=schedule');
    }
    else
    {
        $_SESSION['message'] = "Error! Something Wrong!!";
        header('location: ../clear.php?status=error');
    }
}
elseif ($_GET['from'] == 'courses')
{
    $status = $objDB->undoAll('assign_course');

    if ($status)
    {
        $objTeacher->undoAllReset();
        $_SESSION['message'] = "Data Return Back !!";
        header('location: ../clear.php');
    }
    else
    {
        $_SESSION['message'] = "Error! Something Wrong!!";
        header('location: ../clear.php?status=error');
    }
}
elseif ($_GET['from'] == 'schedule')
{
    $status = $objDB->undoAll('allocate_rooms');

    if ($status)
    {
        $_SESSION['message'] = "Data Return Back !!";
        header('location: ../clear.php');
    }
    else
    {
        $_SESSION['message'] = "Error! Something Wrong!!";
        header('location: ../clear.php?status=error');
    }
}
else
{
    header('location: ../../not-found.php');
}