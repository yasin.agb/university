<?php

include_once ("../../vendor/autoload.php");

use App\Database\Database;
use App\Utility\Utility;

$objDB = new Database();

if (isset($_POST['enroll']) || isset($_POST['result']))
{
    if (isset($_POST['enroll']))
    {
        $aStudent = $objDB->getOne("SELECT students.student_name, students.email, departments.dept_name, departments.id FROM students LEFT JOIN departments ON students.department_id = departments.id WHERE students.id =". $_POST['enroll']);
        $allCourse = $objDB->getAll("SELECT course_name, id FROM courses WHERE dept_id = ".$aStudent['id']);
    }
    elseif (isset($_POST['result']))
    {
        $aStudent = $objDB->getOne("SELECT students.student_name, students.email, departments.dept_name, departments.id FROM students LEFT JOIN departments ON students.department_id = departments.id WHERE students.id =". $_POST['result']);
        $allCourse = $objDB->getAll("SELECT course_name, courses.id FROM courses LEFT JOIN enroll_courses ON enroll_courses.course_id = courses.id WHERE enroll_courses.student_id =". $_POST['result']);
    }

    $allCourse['studentInfo'] = $aStudent;

    echo json_encode($allCourse);
}

if (isset($_POST['showResult']))
{
    $data = $objDB->getAll(
        "select students.student_name, students.email, departments.dept_name, courses.course_code, courses.course_name, grades.grade
                from enroll_courses
                left join grades on grades.id = enroll_courses.grade_id
                inner join courses on enroll_courses.course_id = courses.id
                inner join students on enroll_courses.student_id = students.id
                inner join departments on courses.dept_id = departments.id
                where enroll_courses.student_id =". $_POST['showResult']
    );

    for ($i=0; $i<count($data); $i++)
    {
        if ($data[$i]['grade'] == null)
        {
            $data[$i]['grade'] = "Not Graded Yet";
        }
    }

    echo json_encode($data);
}

if (isset($_POST['assignCourse']))
{
    $techInfo = $objDB->getAll(
        "SELECT teacher_name, teachers.id
                FROM teachers
                WHERE teachers.dept_id =". $_POST['assignCourse']
    );
    $courseInfo = $objDB->getAll(
        "SELECT id, course_code
                FROM courses
                WHERE courses.dept_id =". $_POST['assignCourse']
    );

    $data = [
        'teacher' => $techInfo,
        'course' => $courseInfo
    ];

    echo json_encode($data);
}

if (isset($_POST['teacherInfo']))
{
    $techInfo = $objDB->getOne(
        "SELECT credit_taken, remaning_credit
            FROM teachers
            WHERE id =" .$_POST['teacherInfo']
    );

    echo json_encode($techInfo);
}

if (isset($_POST['courseInfo']))
{
    $courseData = $objDB->getOne(
        "SELECT courses.course_name, courses.credit
            FROM courses
            WHERE id =". $_POST['courseInfo']
    );

    echo json_encode($courseData);
}

if (isset($_POST['showCourse']))
{
    $courseData = $objDB->getAll(
        "SELECT courses.course_code, courses.course_name, semesters.semester, teachers.teacher_name, assign_course.is_delete
                FROM courses
                LEFT JOIN assign_course ON assign_course.course_id = courses.id
                LEFT JOIN semesters ON semesters.id = courses.semester_id
                LEFT JOIN teachers ON teachers.id = assign_course.teacher_id
                WHERE courses.dept_id = ". $_POST['showCourse']
    );

    for ($i=0; $i<count($courseData); $i++)
    {
        if ($courseData[$i]['teacher_name'] == null || $courseData[$i]['is_delete'] == 1)
        {
            $courseData[$i]['teacher_name'] = "<i>Not Assigned Yet</i>";
        }
    }

    echo json_encode($courseData);
}

if (isset($_POST['alloRoom']))
{
    $allCourse = $objDB->getAll("SELECT course_name, id FROM courses WHERE dept_id = ".$_POST['alloRoom']);

    echo json_encode($allCourse);
}

if (isset($_POST['showSchedule']))
{
    $course = $objDB->getAll(
        "SELECT courses.id, courses.course_code, courses.course_name
                FROM courses
                WHERE courses.dept_id = ". $_POST['showSchedule']
    );

    $schedule = $objDB->getAll(
        "SELECT courses.id, rooms.room, days.day, allocate_rooms.start_time, allocate_rooms.end_time
                FROM `allocate_rooms`
                LEFT JOIN courses ON courses.id = allocate_rooms.course_id
                LEFT JOIN rooms ON rooms.id = allocate_rooms.room_id
                LEFT JOIN days ON days.id = allocate_rooms.day_id
                LEFT JOIN departments ON departments.id = courses.dept_id
                WHERE allocate_rooms.is_delete != 1 AND departments.id =". $_POST['showSchedule']
    );

    $data = array();
    $i = 0;
    foreach ($course as $value)
    {
        $data[$i]['courseCode'] = $value['course_code'];
        $data[$i]['courseName'] = $value['course_name'];

        $data[$i]['courseSchedule'] = '';
        foreach ($schedule as $item)
        {
            if ($value['id'] == $item['id'])
            {
                $data[$i]['courseSchedule'] .= "Room No : " .$item['room']. ", " .$item['day']. ", " .Utility::decodeHour($item['start_time']). " - " .Utility::decodeHour($item['end_time']). "<br>";
            }
        }
        if (empty($data[$i]['courseSchedule']))
        {
            $data[$i]['courseSchedule'] = "<i>Not Scheduled Yet</i>";
        }

        $i++;
    }

    echo json_encode($data);
}