<?php

session_start();

include_once ("../../vendor/autoload.php");

use App\Database\Database;
use App\Utility\Utility;

$objDB = new Database();
$status = $objDB->undo($_GET['id'], $_GET['table']);

if ($_GET['table'] == 'departments') {
    if ($status) {
        $_SESSION['message'] = "Data Return Back!!";
        header('location: ../department/departments.php');
    } else {
        $_SESSION['message'] = "Error! Something Wrong!!";
        header('location: ../department/departments.php?status=error');
    }
}