<?php

include_once ("../../vendor/autoload.php");
use App\Auth\Auth;
use App\Utility\Utility;

$objAuth = new Auth();

if (isset($_POST['adminLogin'])) {
    $objAuth->setData($_POST)->loginAdmin();
}
else
{
    header('location: ../../not-found.php');
}