<?php

session_start();

include_once ("../../vendor/autoload.php");

use App\Database\Database;
use App\Utility\Utility;

$objDB = new Database();


if ($_GET['table'] == 'departments')
{
    $status = $objDB->delete($_GET['id'], $_GET['table']);

    if ($status)
    {
        $_SESSION['message'] = "Data Deleted!!";
        header('location: ../department/departments.php?from=delete&id='.$_GET['id']);
    }
    else
    {
        $_SESSION['message'] = "Error! Something Wrong!!";
        header('location: ../department/departments.php?status=error');
    }
}
else
{
    header('location: ../../not-found.php');
}