<?php

include_once ("../../vendor/autoload.php");

use App\Department\Department;
use App\Utility\Utility;

$objDept = new Department();

if (isset($_POST['editDept']))
{
    $_SESSION['formData'] = $_POST;
    $objDept->setData($_POST)->validation()->update();
}