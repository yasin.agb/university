<?php

session_start();

if (isset($_SESSION['admin']))
{
    unset($_SESSION['admin']);
    $_SESSION['message'] = "You are logged out";
    header('location: ../../index.php');
}
else{
    header('location: ../../not-found.php');
}