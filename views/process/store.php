<?php

include_once ("../../vendor/autoload.php");

use App\Department\Department;
use App\Course\Course;
use App\Teacher\Teacher;
use App\Student\Student;
use App\Utility\Utility;
use App\Room\Room;

$objDept = new Department();
$objCourse = new Course();
$objTeacher = new Teacher();
$objStudent = new Student();
$objRoom = new Room();

if (isset($_POST['saveDept']))
{
    $_SESSION['formData'] = $_POST;
    $objDept->setData($_POST)->validation()->store();
}
elseif (isset($_POST['saveCourse']))
{
    $_SESSION['formData'] = $_POST;
    $objCourse->setData($_POST)->validation()->store();
}
elseif (isset($_POST['saveTeacher']))
{
    $_SESSION['formData'] = $_POST;
    $objTeacher->setData($_POST)->validation()->store();
}
elseif (isset($_POST['saveStudent']))
{
    $_SESSION['formData'] = $_POST;
    $objStudent->setData($_POST)->validation()->store();
}
elseif (isset($_POST['enrollCourse']))
{
    $objStudent->setData($_POST)->enrollCourseValidation()->enrollCourse();
}
elseif (isset($_POST['saveResult']))
{
    $objStudent->setData($_POST)->storeResult();
}
elseif (isset($_POST['assignTeacher']))
{
    $_SESSION['assignTeacherData'] = $_POST;
    $objTeacher->setData($_POST)->courseAssignValidation()->courseAssign();
}
elseif (isset($_POST['alloRoom']))
{
    $objRoom->setData($_POST)->validation()->store();
}
else
{
    header('location: ../../not-found.php');
}