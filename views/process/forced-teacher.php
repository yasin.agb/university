<?php

session_start();

if (!isset($_SESSION['admin']))
{
    $_SESSION['message'] = "You need login to continue";
    header('location: ../../not-found.php');
}

include_once("../../vendor/autoload.php");

use App\Teacher\Teacher;
use App\Utility\Utility;

$objTeacher = new Teacher();

if (!isset($_GET['forced']))
{
    header('location: ../../not-found.php');
}

$objTeacher->setData($_SESSION['assignTeacherData'])->courseAssign();
