<?php

include ('../../vendor/mpdf/mpdf/mpdf.php');
include_once ("../../vendor/autoload.php");

use App\Database\Database;
use App\Utility\Utility;

$objDB = new Database();

if (!isset($_GET['stdId']))
{
    header('location: ../../not-found.php');
    die();
}

$data = $objDB->getAll(
    "select students.student_name, students.email, departments.dept_name, courses.course_code, courses.course_name, grades.grade
            from enroll_courses
            left join grades on grades.id = enroll_courses.grade_id
            inner join courses on enroll_courses.course_id = courses.id
            inner join students on enroll_courses.student_id = students.id
            inner join departments on courses.dept_id = departments.id
            where enroll_courses.student_id =". $_GET['stdId']
);

for ($i=0; $i<count($data); $i++)
{
    if ($data[$i]['grade'] == null)
    {
        $data[$i]['grade'] = "Not Graded Yet";
    }
}

$name = $data['0']['student_name'];
$email = $data['0']['email'];
$department = $data['0']['dept_name'];

$tdtr = " ";
foreach ($data as $value):

    $tdtr .= "<tr>";
    $tdtr .= "<td>" . $value['course_code'] . "</td>";
    $tdtr .= "<td>" . $value['course_name'] . "</td>";
    $tdtr .= "<td>" . $value['grade'] . "</td>";
    $tdtr .= "</tr>";

endforeach;


$html = <<<EOD

<!DOCTYPE html>
<head>

    <title>Result | Dynamic University</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../assets/css/sb-admin-2.css" rel="stylesheet">

    <!-- jQuery -->
    <script src="../../assets/js/jquery.min.js"></script>
    
    <style>
        table
        {
        margin-top: 12px;
        }
        th
        {
            padding: 5px 0;
        }
        td
        {
            padding: 5px 0;
            text-align: center;
        }
        h3
        {
            text-align: center;
            text-decoration: underline;
        }
    </style>


</head>

<body>


<h3>Student Result:</h3>
<div>Name : $name </div>
<div>Email : $email </div>
<div>Department : $department </div>
<table class="table" border="1">
    <thead>
        <tr>
            <th>Course Code</th>
            <th>Course Name</th>
            <th>Grade</th>
        </tr>
    </thead>
    
    <tbody>
        $tdtr;
    </tbody>
</table>


<!-- Bootstrap Core JavaScript -->
<script src="../../assets/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../../assets/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../../assets/js/sb-admin-2.js"></script>

</body>

</html>

EOD;

$mpdf = new mPDF();
$mpdf->WriteHTML($html);
$mpdf->Output();
exit();

?>