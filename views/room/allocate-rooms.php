<?php

session_start();

if (!isset($_SESSION['admin']))
{
    $_SESSION['message'] = "You need login to continue";
    header('location: ../../not-found.php');
}

include_once ("../../vendor/autoload.php");

use App\Database\Database;
use App\Utility\Utility;

$objDB = new Database();
$allDept = $objDB->getAll('SELECT * FROM `departments` ORDER BY id DESC');
$allRoom = $objDB->getAll('SELECT * FROM `rooms`');
$allDay = $objDB->getAll('SELECT * FROM `days`');

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Allocate Rooms | Dynamic University</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../assets/css/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../assets/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../../assets/js/jquery.min.js"></script>


</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../../index.php">Dynamic University</a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">

            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="../process/logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li>
                        <a href="#">  Department<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../department/save-department.php">Save Department</a>
                            </li>
                            <li>
                                <a href="../department/departments.php">View Departments</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> Course<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../course/save-course.php">Save Course</a>
                            </li>
                            <li>
                                <a href="../course/courses.php">View Courses</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> Teacher<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../teacher/save-teacher.php">Save Teacher</a>
                            </li>
                            <li>
                                <a href="../teacher/assign-course.php">Assign Course</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> Student<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../student/registration.php">Registration</a>
                            </li>
                            <li>
                                <a href="../student/enroll-course.php">Enroll Course</a>
                            </li>
                            <li>
                                <a href="../student/save-result.php">Save Result</a>
                            </li>
                            <li>
                                <a href="../student/results.php">View Result</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> Classroom<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../room/allocate-rooms.php">Allocate Class</a>
                            </li>
                            <li>
                                <a href="../room/class-schedule.php">Class Schedule</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="../clear.php"> Clear Data</a>
                    </li>


                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
        <!--        <div class="container">-->

        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="login-panel panel panel-default" style="margin-top: 20px;">
                    <div class="panel-heading">
                        <h3 class="panel-title">Allocate Classrooms</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="../process/store.php" method="post">
                            <fieldset>

                                <div class="table-responsive">
                                    <table class="table table-hover" style="border: 0px;">
                                        <tbody>

                                        <?php if (isset($_SESSION['message'] )) {?>
                                            <div class="alert <?php if (isset($_GET['status'])){ if ($_GET['status'] == 'error'){echo "alert-danger";}}else{echo "alert-success";}?> alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <?php
                                                echo $_SESSION['message'];
                                                unset($_SESSION['message']);
                                                ?>
                                            </div>
                                        <?php } ?>

                                        <tr>
                                            <div class="form-group">
                                                <td style="padding-top: 15px;"><label for="dept">Department</label></td>
                                                <td><select class="form-control" name="department" type="text" id="dept" required>
                                                        <option style="text-align: center" value=""> -- Select Department -- </option>
                                                        <?php foreach ($allDept as $value) {?>
                                                            <option style="text-align: center" value="<?php echo $value['id']; ?>"><?php echo $value['dept_name']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </td>
                                            </div>
                                        </tr>

                                        <tr>
                                            <div class="form-group">
                                                <td style="padding-top: 15px;"><label for="course">Course</label></td>
                                                <td><select class="form-control" name="course" type="text" id="course" required>

                                                    </select>
                                                </td>
                                            </div>
                                        </tr>

                                        <tr>
                                            <div class="form-group">
                                                <td style="padding-top: 15px;"><label for="room">Room No</label></td>
                                                <td><select class="form-control" name="room" type="text" id="room" required>
                                                        <option style="text-align: center" value=""> -- Select Room -- </option>
                                                        <?php foreach ($allRoom as $value) {?>
                                                            <option style="text-align: center" value="<?php echo $value['id']; ?>"><?php echo $value['room']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </td>
                                            </div>
                                        </tr>

                                        <tr>
                                            <div class="form-group">
                                                <td style="padding-top: 15px;"><label for="day">Day</label></td>
                                                <td><select class="form-control" name="day" type="text" id="day" required>
                                                        <option style="text-align: center" value=""> -- Select Day -- </option>
                                                        <?php foreach ($allDay as $value) {?>
                                                            <option style="text-align: center" value="<?php echo $value['id']; ?>"><?php echo $value['day']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </td>
                                            </div>
                                        </tr>

                                        <tr>
                                            <div class="form-group">
                                                <td style="padding-top: 15px;"><label for="startTime">Start Time</label></td>
                                                <td>

                                                    <select name="sh" id="startTime" required>
                                                        <option style="text-align: center" value=""> hh </option>
                                                        <?php for ($i=1; $i<=12; $i++) { ?>
                                                        <option style="text-align: center" value="<?php echo $i ?>">
                                                            <?php
                                                            if (strlen($i) < 2) {
                                                                echo "0".$i;
                                                            }
                                                            else
                                                            {
                                                                echo $i;
                                                            }
                                                            ?>
                                                        </option>
                                                        <?php } ?>
                                                    </select>

                                                    <b> : </b>

                                                    <input type="number" name="sm" placeholder="mm" max="59" minlength="2" required>

                                                    <br>
                                                    <input type="radio" name="ss" value="am" required> AM

                                                    <input type="radio" name="ss" value="pm"> PM

                                                </td>
                                            </div>
                                        </tr>

                                        <tr>
                                            <div class="form-group">
                                                <td style="padding-top: 15px;"><label for="name">End Time</label></td>
                                                <td>

                                                    <select name="eh" required>
                                                        <option style="text-align: center" value=""> hh </option>
                                                        <?php for ($i=1; $i<=12; $i++) { ?>
                                                            <option style="text-align: center" value="<?php echo $i ?>">
                                                                <?php
                                                                if (strlen($i) < 2) {
                                                                    echo "0".$i;
                                                                }
                                                                else
                                                                {
                                                                    echo $i;
                                                                }
                                                                ?>
                                                            </option>
                                                        <?php } ?>
                                                    </select>

                                                    <b> : </b>

                                                    <input type="number" name="em" placeholder="mm" max="59" minlength="2" required>

                                                    <br>
                                                    <input type="radio" name="es" value="am" required> AM

                                                    <input type="radio" name="es" value="pm"> PM

                                                </td>
                                            </div>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>

                                <!-- Change this to a button or input when using this as a form -->
                                <input type="submit" class="btn btn-lg btn-success btn-block" value="Allocate" name="alloRoom">
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
    <!--    </div>-->
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


<script>

    $(document).ready(function(){

        $("#dept").change(function(){

            $.ajax({
                url: '../process/ajax.php',
                type: 'post',
                data: {alloRoom:$(this).val()},
                dataType: 'json',
                success:function(response){

                    console.log(response);

                    var len = response.length;

                    $("#course").empty();

                    $("#course").append("<option style='text-align: center' value=''>"+ '-- Select Course --' +"</option>");
                    for( var i = 0; i<len; i++){
                        $("#course").append("<option style='text-align: center' value='"+response[i]['id']+"'>"+response[i]['course_name']+"</option>");
                    }
                }
            });
        });

    });

</script>

<!-- Bootstrap Core JavaScript -->
<script src="../../assets/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../../assets/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../../assets/js/sb-admin-2.js"></script>

</body>

</html>