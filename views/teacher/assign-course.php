<?php

session_start();

if (!isset($_SESSION['admin']))
{
    $_SESSION['message'] = "You need login to continue";
    header('location: ../../not-found.php');
}

if (isset($_SESSION['assignTeacherData']))
{
    unset($_SESSION['assignTeacherData']);
}

include_once ("../../vendor/autoload.php");

use App\Database\Database;
use App\Utility\Utility;

$objDB = new Database();
$allDept = $objDB->getAll('SELECT * FROM `departments` ORDER BY id DESC');

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Save Course | Dynamic University</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../assets/css/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../assets/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../../assets/js/jquery.min.js"></script>


</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../../index.php">Dynamic University</a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">

            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="../process/logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li>
                        <a href="#">  Department<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../department/save-department.php">Save Department</a>
                            </li>
                            <li>
                                <a href="../department/departments.php">View Departments</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> Course<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../course/save-course.php">Save Course</a>
                            </li>
                            <li>
                                <a href="../course/courses.php">View Courses</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> Teacher<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../teacher/save-teacher.php">Save Teacher</a>
                            </li>
                            <li>
                                <a href="../teacher/assign-course.php">Assign Course</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> Student<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../student/registration.php">Registration</a>
                            </li>
                            <li>
                                <a href="../student/enroll-course.php">Enroll Course</a>
                            </li>
                            <li>
                                <a href="../student/save-result.php">Save Result</a>
                            </li>
                            <li>
                                <a href="../student/results.php">View Result</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> Classroom<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../room/allocate-rooms.php">Allocate Class</a>
                            </li>
                            <li>
                                <a href="../room/class-schedule.php">Class Schedule</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="../clear.php"> Clear Data</a>
                    </li>


                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
        <!--        <div class="container">-->

        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="login-panel panel panel-default" style="margin-top: 20px;">
                    <div class="panel-heading">
                        <h3 class="panel-title">Course Assign to Teacher</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="../process/store.php" method="post">
                            <fieldset>

                                <div class="table-responsive">
                                    <table class="table table-hover" style="border: 0px;">
                                        <tbody>

                                        <?php if (isset($_SESSION['message'] )) {?>
                                            <div class="alert <?php if (isset($_GET['status'])){ if ($_GET['status'] == 'error'){echo "alert-danger";}}else{echo "alert-success";}?> alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <?php
                                                echo $_SESSION['message'];
                                                unset($_SESSION['message']);
                                                ?>
                                            </div>
                                        <?php } ?>

                                        <tr>
                                            <div class="form-group">
                                                <td style="padding-top: 15px;"><label for="reg"><sup>*</sup>Department</label></td>
                                                <td><select class="form-control" name="department" id="dept" style="margin-top: 5px;" required>
                                                        <option style="text-align: center" value=""> -- Select a Department -- </option>
                                                        <?php foreach ($allDept as $value) {?>
                                                            <option style="text-align: center" value="<?php echo $value['id']; ?>" > <?php echo $value['dept_name']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </td>
                                            </div>
                                        </tr>

                                        <tr>
                                            <div class="form-group">
                                                <td style="padding-top: 15px;"><label for="teacher"><sup>*</sup>Teacher</label></td>
                                                <td><select class="form-control" name="teacher" type="text" id="teacher" required>
                                                    </select>
                                                </td>
                                            </div>
                                        </tr>

                                        <tr>
                                            <div class="form-group">
                                                <td style="padding-top: 15px;"><label for="totalCredit">Credit to be taken</label></td>
                                                <td><input style="text-align: center; margin-top: 7px;" class="form-control" type="text" id="totalCredit" readonly></td>
                                            </div>
                                        </tr>

                                        <tr>
                                            <div class="form-group">
                                                <td style="padding-top: 15px;"><label for="remainingCredit">Remaining credit</label></td>
                                                <td><input style="text-align: center" class="form-control" name="remainingCredit" type="text" id="remainingCredit" readonly></td>
                                            </div>
                                        </tr>

                                        <tr>
                                            <div class="form-group">
                                                <td style="padding-top: 15px;"><label for="course"><sup>*</sup>Course Code</label></td>
                                                <td><select class="form-control" name="course" type="text" id="course" required>
                                                    </select>
                                                </td>
                                            </div>
                                        </tr>

                                        <tr>
                                            <div class="form-group">
                                                <td style="padding-top: 15px;"><label for="cName">Course Name</label></td>
                                                <td><input style="text-align: center;" class="form-control" type="text" id="cName" readonly></td>
                                            </div>
                                        </tr>

                                        <tr>
                                            <div class="form-group">
                                                <td style="padding-top: 15px;"><label for="cCredit">Course Credit</label></td>
                                                <td><input style="text-align: center;" class="form-control" name="courseCredit" type="text" id="cCredit" readonly></td>
                                            </div>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>

                                <!-- Change this to a button or input when using this as a form -->
                                <input type="submit" class="btn btn-lg btn-success btn-block" value="Save" name="assignTeacher">
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
    <!--    </div>-->
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


<script>

    $(document).ready(function(){

        $("#dept").change(function(){

            $.ajax({
                url: '../process/ajax.php',
                type: 'post',
                data: {assignCourse:$(this).val()},
                dataType: 'json',
                success:function(response){

                    var teachLen = response['teacher'].length;
                    var courseLen = response['course'].length;


                    $("#teacher").empty();
                    $("#teacher").append("<option style='text-align: center' value=''>"+ '-- Select Teacher Name --' +"</option>");

                    for( var i = 0; i<teachLen; i++){
                        $("#teacher").append("<option style='text-align: center' value='"+response['teacher'][i]['id']+"'>"+response['teacher'][i]['teacher_name']+"</option>");
                    }

                    $("#course").empty();
                    $("#course").append("<option style='text-align: center' value=''>"+ '-- Select a Course --' +"</option>");

                    for( var i = 0; i<courseLen; i++){
                        $("#course").append("<option style='text-align: center' value='"+response['course'][i]['id']+"'>"+response['course'][i]['course_code']+"</option>");
                    }
                }
            });
        });


        $("#teacher").change(function(){

            $.ajax({
                url: '../process/ajax.php',
                type: 'post',
                data: {teacherInfo:$(this).val()},
                dataType: 'json',
                success:function(response){

                    $("#totalCredit").val(response['credit_taken']);
                    $("#remainingCredit").val(response['remaning_credit']);
                }
            });
        });


        $("#course").change(function(){

            $.ajax({
                url: '../process/ajax.php',
                type: 'post',
                data: {courseInfo:$(this).val()},
                dataType: 'json',
                success:function(response){

                    $("#cName").val(response['course_name']);
                    $("#cCredit").val(response['credit']);
                }
            });
        });



    });

</script>



<!-- Bootstrap Core JavaScript -->
<script src="../../assets/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../../assets/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../../assets/js/sb-admin-2.js"></script>

</body>

</html>