<?php

session_start();

if (!isset($_SESSION['admin']))
{
    $_SESSION['message'] = "You need login to continue";
    header('location: ../../not-found.php');
}

include_once ("../../vendor/autoload.php");

use App\Database\Database;
use App\Utility\Utility;

$objDB = new Database();
$allDept = $objDB->getAll('SELECT * FROM `departments` WHERE is_delete != 1 ORDER BY id DESC');

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Departments | Dynamic University</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../assets/css/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../assets/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">


</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../../index.php">Dynamic University</a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">

            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="../process/logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li>
                        <a href="#">  Department<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../department/save-department.php">Save Department</a>
                            </li>
                            <li>
                                <a href="../department/departments.php">View Departments</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> Course<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../course/save-course.php">Save Course</a>
                            </li>
                            <li>
                                <a href="../course/courses.php">View Courses</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> Teacher<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../teacher/save-teacher.php">Save Teacher</a>
                            </li>
                            <li>
                                <a href="../teacher/assign-course.php">Assign Course</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> Student<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../student/registration.php">Registration</a>
                            </li>
                            <li>
                                <a href="../student/enroll-course.php">Enroll Course</a>
                            </li>
                            <li>
                                <a href="../student/save-result.php">Save Result</a>
                            </li>
                            <li>
                                <a href="../student/results.php">View Result</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> Classroom<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../room/allocate-rooms.php">Allocate Class</a>
                            </li>
                            <li>
                                <a href="../room/class-schedule.php">Class Schedule</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="../clear.php"> Clear Data</a>
                    </li>


                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
        <!--        <div class="container">-->


        <div class="row">
            <div class="col-lg-12" style="padding-top: 20px;">

                <?php if (isset($_SESSION['message'] )) {?>
                    <div style="text-align: center" class="alert <?php if (isset($_GET['status'])){ if ($_GET['status'] == 'error'){echo "alert-danger";}}else{echo "alert-success";}?> alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php
                        $padding = 1;
                        echo $_SESSION['message']; if (isset($_GET['from'])) { if ($_GET['from'] == 'delete') {?>
                        <a href="../process/undo.php?id=<?php echo $_GET['id'] ?>&table=departments"> Undo</a>
                        <?php }}; unset($_SESSION['message']);
                        ?>
                    </div>
                <?php } ?>

            </div>
        </div>

        <div class="row">
            <div class="col-lg-12" <?php if (!isset($padding)) { echo "style='padding-top: 20px'"; } ?> >


                <div class="panel panel-default" >

                    <div class="panel-heading">
                        <strong>Departments</strong>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $serial = 1; foreach ($allDept as $value) {?>

                                <tr>
                                    <td><?php echo $serial++; ?></td>
                                    <td><?php echo $value['dept_code']; ?></td>
                                    <td><?php echo $value['dept_name']; ?></td>
                                    <td>
                                            <a type="button" href="edit-department.php?id=<?php echo $value['id']; ?>" class="btn btn-outline btn-primary btn-sm">Edit</a>
                                        ||
                                            <a type="button" href="../confirm/delete.php?id=<?php echo $value['id']; ?>&table=departments" class="btn btn-outline btn-danger btn-sm">Delete</a>

                                    </td>
                                </tr>

                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>

            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
    <!--    </div>-->
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../../assets/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../../assets/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../../assets/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../../assets/js/sb-admin-2.js"></script>

</body>

</html>
