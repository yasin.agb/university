<?php

session_start();

if (!isset($_SESSION['admin']))
{
    $_SESSION['message'] = "You need login to continue";
    header('location: ../../not-found.php');
}

include_once("../../vendor/autoload.php");

use App\Utility\Utility;


if (!isset($_GET['id']))
{
    header('location: ../../not-found.php');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Delete | Dynamic University</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../assets/css/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../assets/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">


</head>

<body>


<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="../../index.php">Dynamic University</a>
    </div>

</nav>


<div class="modal-dialog" style="padding-top: 100px; width: 500px;">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel" style="color: red">Delete!! Are you sure?</h4>
        </div>
        <div class="modal-footer">
            <a href="../<?php

            if (isset($_GET['table'])) {
                if ($_GET['table'] == 'departments') {
                    echo "department/departments.php";
                }
            }

            ?>" style="text-decoration: none">
            <button type="button" class="btn btn-success" data-dismiss="modal">No, Back</button>
            </a>
            <a href="../process/delete.php?table=<?php echo $_GET['table'] ?>&id=<?php echo $_GET['id'] ?>" style="text-decoration: none">
                <button type="button" class="btn btn-danger">Sure, Delete it</button>
            </a>
        </div>
    </div>
</div>


<!-- jQuery -->
<script src="../../assets/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../../assets/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../../assets/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../../assets/js/sb-admin-2.js"></script>

</body>

</html>
