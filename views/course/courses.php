<?php

session_start();

if (!isset($_SESSION['admin']))
{
    $_SESSION['message'] = "You need login to continue";
    header('location: ../../not-found.php');
}

include_once ("../../vendor/autoload.php");

use App\Database\Database;
use App\Utility\Utility;

$objDB = new Database();
$allDept = $objDB->getAll('SELECT * FROM `departments` ORDER BY id DESC');

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Courses | Dynamic University</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../assets/css/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../assets/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../../assets/js/jquery.min.js"></script>


</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../../index.php">Dynamic University</a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">

            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="../process/logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li>
                        <a href="#">  Department<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../department/save-department.php">Save Department</a>
                            </li>
                            <li>
                                <a href="../department/departments.php">View Departments</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> Course<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../course/save-course.php">Save Course</a>
                            </li>
                            <li>
                                <a href="../course/courses.php">View Courses</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> Teacher<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../teacher/save-teacher.php">Save Teacher</a>
                            </li>
                            <li>
                                <a href="../teacher/assign-course.php">Assign Course</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> Student<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../student/registration.php">Registration</a>
                            </li>
                            <li>
                                <a href="../student/enroll-course.php">Enroll Course</a>
                            </li>
                            <li>
                                <a href="../student/save-result.php">Save Result</a>
                            </li>
                            <li>
                                <a href="../student/results.php">View Result</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> Classroom<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../room/allocate-rooms.php">Allocate Class</a>
                            </li>
                            <li>
                                <a href="../room/class-schedule.php">Class Schedule</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="../clear.php"> Clear Data</a>
                    </li>


                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
        <!--        <div class="container">-->

        <div class="row">
            <div class="col-lg-12" style="padding-top: 20px;">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Selects Department :</label>
                            <select class="form-control" id="dept">
                                <option style="text-align: center" value=""> -- Select Department -- </option>
                                <?php foreach ($allDept as $value) {?>
                                    <option style="text-align: center" value="<?php echo $value['id']; ?>"><?php echo $value['dept_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Courses</strong>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">

                            <table class="table table-striped" id="table">
                                <thead>
                                <tr>
                                    <td style="font-weight: 600">Select a Department</td>
                                </tr>
                                </thead>
                            </table>

                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>

            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
    <!--    </div>-->
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


<script>

    $(document).ready(function(){

        $("#dept").change(function(){

            $.ajax({
                url: '../process/ajax.php',
                type: 'post',
                data: {showCourse:$(this).val()},
                dataType: 'json',
                success:function(response){

                    var len = response.length;

                    $("#table").empty();

                    $("#table").append("<thead><tr><th>#</th><th>Course Code</th><th>Course Name</th><th>Semester</th><th>Assigned To</th></tr></thead><tbody>");

                    for( var i = 0; i<len; i++){
                        $("#table").append("<tr><td>"+ (i+1) +"</td><td>"+ response[i]['course_code'] +"</td><td>"+ response[i]['course_name'] +"</td></td><td>"+ response[i]['semester'] +"</td><td>"+ response[i]['teacher_name'] +"</td></tr>");
                    }

                    $("#table").append("</tbody>");
                }
            });
        });

    });

</script>


<!-- Bootstrap Core JavaScript -->
<script src="../../assets/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../../assets/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../../assets/js/sb-admin-2.js"></script>

</body>

</html>
