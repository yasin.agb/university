-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 12, 2017 at 10:44 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `university`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `email`, `password`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin@gmail.com', '123', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `allocate_rooms`
--

CREATE TABLE `allocate_rooms` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `day_id` int(11) NOT NULL,
  `start_time` int(11) NOT NULL,
  `end_time` int(11) NOT NULL,
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `allocate_rooms`
--

INSERT INTO `allocate_rooms` (`id`, `course_id`, `room_id`, `day_id`, `start_time`, `end_time`, `is_delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 5, 3, 500, 700, 0, '2017-05-12 10:05:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 5, 7, 4, 930, 1100, 0, '2017-05-12 10:05:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 5, 8, 2, 1500, 1630, 0, '2017-05-12 10:05:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 10, 7, 7, 1500, 1800, 0, '2017-05-12 10:05:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 11, 9, 2, 1145, 1300, 0, '2017-05-12 10:05:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 14, 5, 6, 1800, 2000, 0, '2017-05-12 10:05:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `assign_course`
--

CREATE TABLE `assign_course` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assign_course`
--

INSERT INTO `assign_course` (`id`, `teacher_id`, `course_id`, `is_delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 2, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 4, 6, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 5, 4, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 7, 9, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 8, 10, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 10, 13, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 10, 14, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 9, 12, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `course_code` varchar(255) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `credit` float NOT NULL,
  `description` text NOT NULL,
  `dept_id` int(11) NOT NULL,
  `semester_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `course_code`, `course_name`, `credit`, `description`, `dept_id`, `semester_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'BM-124', 'Business Mathematics', 3, '', 1, 5, '2017-05-12 09:05:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'AF-657', 'Micro Economics', 4, '', 1, 8, '2017-05-12 09:05:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'UH-328', 'Cost & Management Accounting', 2, '', 1, 1, '2017-05-12 09:05:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'AR-325', 'Industrial Marketing', 4, '', 2, 2, '2017-05-12 09:05:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'YA-172', 'Business Communication', 3, '', 2, 3, '2017-05-12 09:05:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'EN-394', 'Business Environment', 3, '', 2, 6, '2017-05-12 09:05:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'ME-418', 'Human Resourse Planning', 2.5, '', 2, 8, '2017-05-12 09:05:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'NN-314', 'Circuit Theory & Networks', 3, '', 3, 3, '2017-05-12 10:05:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'UA-951', 'Materials Science', 4, '', 3, 4, '2017-05-12 10:05:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'BE-237', 'Analog Electronic Circuits', 4.25, '', 3, 6, '2017-05-12 10:05:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'KA-951', 'Muslim Personal Law', 3, '', 4, 3, '2017-05-12 10:05:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'RV-683', 'Public International Law', 3, '', 4, 4, '2017-05-12 10:05:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'MB-417', 'Special And Local Laws', 2.2, '', 4, 7, '2017-05-12 10:05:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'RF-415', 'Law Of Civil Procedure', 3, '', 4, 5, '2017-05-12 10:05:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `days`
--

CREATE TABLE `days` (
  `id` int(11) NOT NULL,
  `day` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `days`
--

INSERT INTO `days` (`id`, `day`) VALUES
(1, 'Saturday'),
(2, 'Sunday'),
(3, 'Monday'),
(4, 'Tuesday'),
(5, 'Wednesday'),
(6, 'Thursday'),
(7, 'Friday');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `dept_code` varchar(255) NOT NULL,
  `dept_name` varchar(255) NOT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `dept_code`, `dept_name`, `is_delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'BBA-214', 'Bachelor Of Business Administration', 0, '2017-05-12 09:05:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'MBA-285', 'Master Of Business Administration', 0, '2017-05-12 09:05:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'EEE-152', 'Electrical And Electronics Engineering', 0, '2017-05-12 09:05:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'LLB-114', 'Literally Legum Baccalaureus', 0, '2017-05-12 09:05:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(11) NOT NULL,
  `designation` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `designation`) VALUES
(1, 'Dr.'),
(2, 'Prof.');

-- --------------------------------------------------------

--
-- Table structure for table `enroll_courses`
--

CREATE TABLE `enroll_courses` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enroll_courses`
--

INSERT INTO `enroll_courses` (`id`, `student_id`, `course_id`, `grade_id`, `date`, `is_delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 1, 0, NULL, 0, '2017-05-12 10:05:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 7, 9, 9, NULL, 0, '2017-05-12 10:05:51', '2017-05-12 10:05:23', '0000-00-00 00:00:00'),
(3, 9, 11, 12, NULL, 0, '2017-05-12 10:05:00', '2017-05-12 10:05:37', '0000-00-00 00:00:00'),
(4, 9, 14, 0, NULL, 0, '2017-05-12 10:05:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 10, 13, 0, NULL, 0, '2017-05-12 10:05:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 4, 5, 0, NULL, 0, '2017-05-12 10:05:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` int(11) NOT NULL,
  `grade` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `grade`) VALUES
(1, 'A+'),
(2, 'A'),
(3, 'A-'),
(4, 'B+'),
(5, 'B'),
(6, 'B-'),
(7, 'C+'),
(8, 'C'),
(9, 'C-'),
(10, 'D+'),
(11, 'D'),
(12, 'D-'),
(13, 'F');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `room` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `room`) VALUES
(1, 101),
(2, 102),
(3, 201),
(4, 202),
(5, 301),
(6, 302),
(7, 401),
(8, 402),
(9, 501),
(10, 502);

-- --------------------------------------------------------

--
-- Table structure for table `semesters`
--

CREATE TABLE `semesters` (
  `id` int(11) NOT NULL,
  `semester` varchar(255) NOT NULL,
  `is_delete` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `semesters`
--

INSERT INTO `semesters` (`id`, `semester`, `is_delete`) VALUES
(1, '1st Semester', 1),
(2, '2nd Semester', 1),
(3, '3rd Semester', 1),
(4, '4th Semester', 1),
(5, '5th Semester', 1),
(6, '6th Semester', 1),
(7, '7th Semester', 1),
(8, '8th Semester', 1);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `registration_no` varchar(255) NOT NULL,
  `student_name` varchar(255) NOT NULL,
  `department_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact_no` varchar(15) DEFAULT NULL,
  `date` date NOT NULL,
  `address` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted-at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `registration_no`, `student_name`, `department_id`, `email`, `contact_no`, `date`, `address`, `created_at`, `updated_at`, `deleted-at`) VALUES
(1, 'BBA-2017-001', 'Asad Hossain', 1, 'asad@gmail.com', NULL, '2017-05-12', '', '2017-05-12 10:05:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'BBA-2017-002', 'Zihad Sikder', 1, 'zihad@yahoo.com', NULL, '2017-05-12', '', '2017-05-12 10:05:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'MBA-2017-001', 'Mohammad Ali', 2, 'ali@mail.com', NULL, '2017-05-12', '', '2017-05-12 10:05:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'MBA-2017-002', 'Abdul Kader', 2, 'kader@ymail.com', NULL, '2017-05-12', '', '2017-05-12 10:05:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'MBA-2017-003', 'Sharif Khan', 2, 'sharif@live.com', NULL, '2017-05-12', '', '2017-05-12 10:05:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'EEE-2017-001', 'Omio Hassan', 3, 'omio@gmail.com', NULL, '2017-05-12', '', '2017-05-12 10:05:21', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'EEE-2017-002', 'Jubayer Ahmed', 3, 'jubayer@yahoo.com', NULL, '2017-05-12', '', '2017-05-12 10:05:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'LLB-2017-001', 'Zahid Al Belal', 4, 'belal@live.com', NULL, '2017-05-12', '', '2017-05-12 10:05:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'LLB-2017-002', 'Mohiuddin Chowdhury', 4, 'chowdhury@mail.com', NULL, '2017-05-12', '', '2017-05-12 10:05:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'LLB-2017-003', 'Sajjad Hasan', 4, 'sajjad@live.com', NULL, '2017-05-12', '', '2017-05-12 10:05:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(11) NOT NULL,
  `teacher_name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact_no` varchar(255) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `credit_taken` float NOT NULL,
  `remaning_credit` float NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `teacher_name`, `address`, `email`, `contact_no`, `designation_id`, `dept_id`, `credit_taken`, `remaning_credit`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Ikram Mansur', '', 'ad@drg.com', '', 0, 1, 10, 10, '2017-05-12 10:05:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Taib Islam Dipu', '', 'ad@rdf.br', '', 0, 1, 12, 8, '2017-05-12 10:05:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Masud Rana', '', 'daw@thgc.dre', '', 0, 2, 9, 9, '2017-05-12 10:05:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Jahidul Kabir', '', 'afa@rh.dhr', '', 0, 2, 10, 7, '2017-05-12 10:05:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Sohan Ahmed', '', 'wagt@gdrh.rh', '', 0, 2, 12, 8, '2017-05-12 10:05:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Shahriar Shuvo', '', 'ada@rh.xe', '', 0, 3, 8, 8, '2017-05-12 10:05:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Sozal Khan', '', 'awf@ftn.red', '', 0, 3, 12, 8, '2017-05-12 10:05:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Hasan Chowdhury', '', 'fyh@tfn.sa', '', 0, 3, 13, 8.75, '2017-05-12 10:05:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Riaz Ahmed', '', 'afwgses@jrt.fdg', '', 0, 4, 10, 7, '2017-05-12 10:05:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Shadhin Khan', '', 'age@ad.iubu', '', 0, 4, 14, 8.8, '2017-05-12 10:05:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `allocate_rooms`
--
ALTER TABLE `allocate_rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assign_course`
--
ALTER TABLE `assign_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `days`
--
ALTER TABLE `days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enroll_courses`
--
ALTER TABLE `enroll_courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `semesters`
--
ALTER TABLE `semesters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `allocate_rooms`
--
ALTER TABLE `allocate_rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `assign_course`
--
ALTER TABLE `assign_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `days`
--
ALTER TABLE `days`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `enroll_courses`
--
ALTER TABLE `enroll_courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `semesters`
--
ALTER TABLE `semesters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
