<?php

namespace App\Room;

use App\Database\Database;
use App\Utility\Utility;

class Room extends Database
{
    private $room;
    private $day;
    private $department;
    private $course;
    private $startTime;
    private $endTime;

    public function setData($data = '')
    {
        $this->room = $data['room'];
        $this->day = $data['day'];
        $this->department = $data['department'];
        $this->course = $data['course'];
        $this->startTime = Utility::encodeHour($data['sh'], $data['sm'], $data['ss']);
        $this->endTime = Utility::encodeHour($data['eh'], $data['em'], $data['es']);

        return $this;
    }

    public  function store()
    {
        $stmt = $this->connect()->prepare('INSERT INTO allocate_rooms(course_id, room_id, day_id, start_time, end_time, created_at)
                                                    VALUES(:cid, :rid, :did, :sTime, :eTime, :cat)');

        $status = $stmt->execute(
            array(
                ':cid' => $this->course,
                ':rid' => $this->room,
                ':did' => $this->day,
                ':sTime' => $this->startTime,
                ':eTime' => $this->endTime,
                ':cat' => date('Y-m-d h:m:s')
            )
        );

        if ($status) {
            $_SESSION['message'] = "Data Saved";
            $_SESSION['studentRegistered'] = '';
            header('location: ../room/allocate-rooms.php');
        } else {
            $_SESSION['message'] = "Error! Something Wrong!!";
            header('location: ../room/allocate-rooms.php?status=error');
        }
    }

    public function validation()
    {
        $schedule = $this->getAll("SELECT start_time, end_time FROM allocate_rooms WHERE room_id = $this->room AND day_id = $this->day ");

        if ($this->startTime > $this->endTime)
        {
            $_SESSION['message'] = "Invalid Time!!";
            header('location: ../room/allocate-rooms.php?status=error');
            die();
        }

        if (!empty($schedule))
        {
            $time = array();
            foreach ($schedule as $value)
            {
                for ($i = 0; $i < 2400; $i++)
                {
                    $less = substr($i, -2, 1);
                    if ($less != 6 && $less != 7 && $less != 8 && $less != 9)
                    {
                        if ($value['start_time'] <= $i && $i <= $value['end_time'])
                        {
                            $time[$i] = 1;
                        }
                    }
                }
            }


            for ($i = 0; $i < 2400; $i++)
            {
                if ($this->startTime <= $i && $i <= $this->endTime)
                {
                    if (isset($time[$i]))
                    {
                        foreach ($schedule as $value)
                        {
                            if ($value['start_time'] <= $i && $i <= $value['end_time'])
                            {
                                $_SESSION['message'] = "This room is not available from " . Utility::decodeHour($value['start_time']) . " to " . Utility::decodeHour($value['end_time']) . " !!";
                                header('location: ../room/allocate-rooms.php?status=error');
                                die();
                            }
                        }
                    }
                }
            }
        }

        return $this;
    }
}