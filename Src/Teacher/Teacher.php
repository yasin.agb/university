<?php

namespace App\Teacher;

use App\Database\Database;
use App\Utility\Utility;

class Teacher extends Database
{
    private $id;
    private $name;
    private $address;
    private $email;
    private $contact;
    private $designation;
    private $department;
    private $credit;
    private $teacher;
    private $course;
    private $courseCredit;
    private $remainingCredit;

    public function setData($data = "")
    {

        if(!empty($data['id'])) {
            $this->id = $data['id'];
        }
        if(!empty($data['name'])) {
            $this->name = ucwords(strtolower($data['name']));
        }
        if(!empty($data['address'])) {
            $this->address = $data['address'];
        }
        if(!empty($data['email'])) {
            $this->email = $data['email'];
        }
        if(!empty($data['contact'])) {
            $this->contact = $data['contact'];
        }
        if(!empty($data['designation'])) {
            $this->designation = $data['designation'];
        }
        if(!empty($data['department'])) {
            $this->department = $data['department'];
        }
        if(!empty($data['credit'])) {
            $this->credit = $data['credit'];
        }
        if(!empty($data['teacher'])) {
            $this->teacher = $data['teacher'];
        }
        if(!empty($data['course'])) {
            $this->course = $data['course'];
        }
        if(!empty($data['courseCredit'])) {
            $this->courseCredit = $data['courseCredit'];
        }
        if(!empty($data['remainingCredit'])) {
            $this->remainingCredit = $data['remainingCredit'];
        }

        return $this;
    }

    public function store()
    {
        $stmt = $this->connect()->prepare('INSERT INTO teachers(teacher_name, address, email, contact_no, designation_id, dept_id, credit_taken, remaning_credit, created_at)
                                                    VALUES(:tName, :address, :email, :contact, :desId, :deptId, :credit, :remaning, :cat)');

        $status = $stmt->execute(
            array(
                ':tName' => $this->name,
                ':address' => $this->address,
                ':email' => $this->email,
                ':contact' => $this->contact,
                ':desId' => $this->designation,
                ':deptId' => $this->department,
                ':credit' => $this->credit,
                ':remaning' => $this->credit,
                ':cat' => date('Y-m-d h:m:s')
            )
        );

        if ($status) {
            $_SESSION['message'] = "Data Saved";
            header('location: ../teacher/save-teacher.php');
        }
        else
        {
            $_SESSION['message'] = "Error! Something Wrong!!";
            header('location: ../teacher/save-teacher.php?status=error');
        }
    }

    public  function courseAssign()
    {
        $teachRemaning = $this->getOne("SELECT remaning_credit FROM teachers WHERE id = $this->teacher");
        $courseCredit = $this->getOne("SELECT credit FROM courses WHERE id = $this->course");

        $rCredit = $teachRemaning['remaning_credit'] - $courseCredit['credit'];

        $stmt = $this->connect()->prepare('INSERT INTO assign_course (teacher_id, course_id) VALUES (:tid, :cid);
                                                    UPDATE teachers SET remaning_credit = :rCredit WHERE id = :tid ');

        $status = $stmt->execute(
            array(
                ':cid' => $this->course,
                ':tid' => $this->teacher,
                ':rCredit' => $rCredit
            )
        );

        if ($status) {
            $_SESSION['message'] = "Data Saved";
            header('location: ../teacher/assign-course.php');
        }
        else
        {
            $_SESSION['message'] = "Error! Something Wrong!!";
            header('location: ../teacher/assign-course.php?status=error');
        }
    }




    public function validation()
    {
        $allTeacher = $this->getAll('SELECT * FROM `teachers`');

        if (empty($this->name)) //Name
        {
            $_SESSION['message'] = "Name can't be empty !!";
            header('location: ../teacher/save-teacher.php?status=error ');
            die();
        }
        elseif (!preg_match("/^[a-zA-Z ]*$/", Utility::test_input($this->name)))
        {
            $_SESSION['message'] = "Only letters and space allowed !!";
            header('location: ../teacher/save-teacher.php?status=error ');
            die();
        }

        if (empty($this->address)) //address
        {
            $this->address = '';
        }
        else
        {
            $this->address = htmlspecialchars($this->address);
        }

        //email
        if (!preg_match("/^[a-zA-Z@.0-9]*$/", Utility::test_input($this->email)))
        {
            $_SESSION['message'] = "Invalid email";
            header('location: ../teacher/save-teacher.php?status=error');
            die();
        }
        else
        {
            foreach ($allTeacher as $value)
            {
                if ($value['email'] == $this->email) {
                    $_SESSION['message'] = "This email already exist !!";
                    header('location: ../teacher/save-teacher.php?status=error');
                    die();
                }
            }
        }

        if (empty($this->contact)) //contact
        {
            $this->contact = '';
        }
        elseif (!preg_match("/^[0-9]*$/", Utility::test_input($this->contact)))
        {
            $_SESSION['message'] = "Contact No.. must be digit only !!";
            header('location: ../teacher/save-teacher.php?status=error');
            die();
        }
        elseif ($this->contact < 1)
        {
            $_SESSION['message'] = "Contact No.. must be Non-negative !!";
            header('location: ../teacher/save-teacher.php?status=error');
            die();
        }

        if (empty($this->designation)) //designation
        {
            $this->designation = '';
        }

        if (empty($this->department)) //department
        {
            $_SESSION['message'] = "Department can't be empty !!";
            header('location: ../teacher/save-teacher.php?status=error');
            die();
        }

        if (empty($this->credit)) //credit
        {
            $_SESSION['message'] = "Credit can't be empty !!";
            header('location: ../teacher/save-teacher.php?status=error');
            die();
        }
        elseif ($this->credit < 1)
        {
            $_SESSION['message'] = "Credit must be positive!!";
            header('location: ../teacher/save-teacher.php?status=error');
            die();
        }

        unset($_SESSION['formData']);
        return $this;
    }

    public function courseAssignValidation()
    {
        $aCourse = $this->getOne("SELECT *
                                    FROM assign_course
                                    WHERE assign_course.course_id = $this->course AND assign_course.is_delete = 0");

        if (!empty($aCourse))
        {
            unset($_SESSION['assignTeacherData']);

            $_SESSION['message'] = "This course already assign to a teacher!!";
            header('location: ../teacher/assign-course.php?status=error');
            die();
        }
        elseif ($this->remainingCredit < $this->courseCredit)
        {
            header('location: ../confirm/force-teacher.php?force');
            die();
        }

        unset($_SESSION['assignTeacherData']);
        return $this;
    }

    public function resetAllTeacher()
    {
        $stmt = $this->connect()->prepare(
            'UPDATE teachers
                        SET teachers.remaning_credit = teachers.credit_taken'
        );

        $stmt->execute();
    }

    public function undoAllReset()
    {
        $allTeacher = $this->getAll(
            "SELECT teachers.id, teachers.credit_taken
                    FROM teachers"
        );

        foreach ($allTeacher as $value)
        {
            $sumOfAssingCoursesCredit = $this->getOne(
                "SELECT SUM(courses.credit)
                    FROM assign_course
                    LEFT JOIN courses ON assign_course.course_id = courses.id
                    WHERE assign_course.teacher_id =". $value['id']
            );

            $remaining = $value['credit_taken'] - $sumOfAssingCoursesCredit['SUM(courses.credit)'];

            if (!empty($sumOfAssingCoursesCredit['SUM(courses.credit)']))
            {
                $stmt = $this->connect()->prepare(
                    "UPDATE teachers
                        SET teachers.remaning_credit = $remaining
                        WHERE teachers.id = " . $value['id']
                );

                $stmt->execute();
            }
        }
    }
}