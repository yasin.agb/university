<?php

namespace App\Utility;


class Utility
{
    public static function dd($data = "")
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        die();
    }

    public static function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    public static function encodeHour($hh, $mm, $apm)
    {
        $time = date("H:i", strtotime("$hh:$mm $apm"));
        $encode = str_replace(":", "", $time);

        return $encode;
    }

    public static function decodeHour($time)
    {
        $time = substr_replace( $time, ':', -2, 0 );
        $decode = date("g:i a", strtotime("$time"));
        $decode = strtoupper($decode);

        return $decode;
    }
}