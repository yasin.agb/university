<?php

namespace App\Auth;

use App\Database\Database;
use App\Utility\Utility;

session_start();

class Auth extends Database
{
    private $email;
    private $password;

    public function setData($data = "")
    {
        if(!empty($data['email'])) {
            $this->email = $data['email'];

        }
        if(!empty($data['password'])) {
            $this->password = $data['password'];
        }
        return $this;
    }

    public function loginAdmin()
    {
        $result = $this->getOne("SELECT * FROM `admins` WHERE email='$this->email' AND password='$this->password'");

        if (!empty($result))
        {
            $_SESSION['admin'] = $result;
            header('location: ../department/save-department.php');
        }
        else
        {
            $_SESSION['message'] = "Email or password not match";
            header('location: ../../not-found.php');
        }
    }
}