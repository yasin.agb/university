<?php

namespace App\Student;

use App\Database\Database;
use App\Utility\Utility;

class Student extends Database
{
    private $id;
    private $name;
    private $address;
    private $email;
    private $contact;
    private $date;
    private $department;
    private $course;
    private $grade;

    public function setData($data = "")
    {

        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (!empty($data['name'])) {
            $this->name = ucwords(strtolower($data['name']));
        }
        if (!empty($data['address'])) {
            $this->address = $data['address'];
        }
        if (!empty($data['email'])) {
            $this->email = $data['email'];
        }
        if (!empty($data['contact'])) {
            $this->contact = $data['contact'];
        }

        if (!empty($data['date'])) {
            $this->date = $data['date'];
        }

        if (!empty($data['department'])) {
            $this->department = $data['department'];
        }
        if (!empty($data['course'])) {
            $this->course = $data['course'];
        }
        if (!empty($data['grade'])) {
            $this->grade = $data['grade'];
        }
        return $this;
    }

    public function store()
    {
        $aDept = $this->getOne("SELECT * FROM `departments` WHERE id = $this->department");
        $allStudent = $this->getAll("SELECT * FROM `students` WHERE department_id = $this->department");

        $deptCodeStr = $result = preg_replace("/[^A-Z]+/", "", $aDept['dept_code']);

        $regSerial = count($allStudent)+1;
        if (strlen($regSerial) == 1)
        {
            $regSerial = '00'.$regSerial;
        }
        elseif (strlen($regSerial) == 2)
        {
            $regSerial = '0'.$regSerial;
        }

        $stmt = $this->connect()->prepare('INSERT INTO students(registration_no, student_name, department_id, email, contact_no, date, address, created_at)
                                                    VALUES(:reg, :sName, :deptId, :email, :contact, :date, :address, :cat)');

        $status = $stmt->execute(
            array(
                ':reg' => $deptCodeStr. "-" .date(Y).'-'.$regSerial,
                ':sName' => $this->name,
                ':deptId' => $this->department,
                ':email' => $this->email,
                ':contact' => $this->contact,
                ':date' => $this->date,
                ':address' => $this->address,
                ':cat' => date('Y-m-d h:m:s')
            )
        );

        if ($status) {
            $_SESSION['message'] = "Student Registered";
            $_SESSION['studentRegistered'] = '';
            header('location: ../student/ok-registration.php');
        } else {
            $_SESSION['message'] = "Error! Something Wrong!!";
            header('location: ../student/registration.php?status=error');
        }
    }

    public function enrollCourse()
    {
        $stmt = $this->connect()->prepare('INSERT INTO enroll_courses(student_id, course_id, date, created_at)
                                                    VALUES(:sid, :cid, :date, :cat)');

        $status = $stmt->execute(
            array(
                ':sid' => $this->id,
                ':cid' => $this->course,
                ':date' => $this->date,
                ':cat' => date('Y-m-d h:m:s')
            )
        );

        if ($status) {
            $_SESSION['message'] = "Data Saved";
            header('location: ../student/enroll-course.php');
        } else {
            $_SESSION['message'] = "Error! Something Wrong!!";
            header('location: ../student/enroll-course.php?status=error');
        }
    }

    public function storeResult()
    {
        $stmt = $this->connect()->prepare('UPDATE enroll_courses SET grade_id = :grade, updated_at = :uat WHERE student_id = :sid AND course_id = :cid');

        $status = $stmt->execute(
            array(
                ':sid' => $this->id,
                ':cid' => $this->course,
                ':grade' => $this->grade,
                ':uat' => date('Y-m-d h:m:s')
            )
        );

        if ($status) {
            $_SESSION['message'] = "Data Saved";
            header('location: ../student/save-result.php');
        } else {
            $_SESSION['message'] = "Error! Something Wrong!!";
            header('location: ../student/save-result.php?status=error');
        }
    }

    public function validation()
    {
        $allStudent = $this->getAll('SELECT * FROM `students`');

        if (empty($this->name)) //Name
        {
            $_SESSION['message'] = "Name can't be empty !!";
            header('location: ../student/registration.php?status=error ');
            die();
        }
        elseif (!preg_match("/^[a-zA-Z ]*$/", Utility::test_input($this->name)))
        {
            $_SESSION['message'] = "Only letters and space allowed !!";
            header('location: ../student/registration.php?status=error ');
            die();
        }

        //email
        if (!preg_match("/^[a-zA-Z @.0-9]*$/", Utility::test_input($this->email)) || !filter_var(Utility::test_input($this->email), FILTER_VALIDATE_EMAIL))
        {
            $_SESSION['message'] = "Invalid email";
            header('location: ../student/registration.php?status=error ');
            die();
        }
        else
        {
            foreach ($allStudent as $value)
            {
                if ($value['email'] == $this->email) {
                    $_SESSION['message'] = "This email already exist !!";
                    header('location: ../student/registration.php?status=error ');
                    die();
                }
            }
        }

        if (!empty($this->contact)) //contact
        {
            if (!preg_match("/^[0-9]*$/", Utility::test_input($this->contact)))
            {
                $_SESSION['message'] = "Contact No.. must be digit only !!";
                header('location: ../student/registration.php?status=error ');
                die();
            }
            elseif ($this->contact < 1)
            {
                $_SESSION['message'] = "Contact No.. must be Non-negative !!";
                header('location: ../student/registration.php?status=error ');
                die();
            }
        }

        $testDate = explode('-', $this->date); //date
        if (empty($this->date))
        {
            $this->date = '';
        }
        elseif (!checkdate($testDate[1], $testDate[2], $testDate[0]))
        {
            $_SESSION['message'] = "Invalid date !!";
            header('location: ../student/registration.php?status=error ');
            die();
        }

        if (empty($this->address)) //address
        {
            $this->address = '';
        }
        else
        {
            $this->address = htmlspecialchars($this->address);
        }

        if (empty($this->department)) //department
        {
            $_SESSION['message'] = "Department can't be empty !!";
            header('location: ../student/registration.php?status=error');
            die();
        }

        unset($_SESSION['formData']);
        return $this;
    }

    public function enrollCourseValidation()
    {
        $check = $this->getOne("SELECT * FROM enroll_courses WHERE enroll_courses.student_id = $this->id AND enroll_courses.course_id = $this->course");

        if (!empty($check))
        {
            $_SESSION['message'] = "This student allready enrolled in this course !!";
            header('location: ../student/enroll-course.php?status=error');
            die();
        }

        return $this;
    }
}