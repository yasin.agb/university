<?php
namespace App\Department;

use App\Database\Database;
use App\Utility\Utility;

session_start();

class Department extends Database
{
    private $id;
    private $code;
    private $name;

    public function setData($data = "")
    {
        if(!empty($data['code'])) {
            $this->code = strtoupper($data['code']);

        }
        if(!empty($data['name'])) {
            $this->name = ucwords(strtolower($data['name']));
        }
        if(!empty($data['id'])) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store()
    {
        $stmt = $this->connect()->prepare('INSERT INTO departments(dept_code, dept_name, created_at) VALUES(:code, :dName, :cat)');

        $status = $stmt->execute(
            array(
                ':code' => $this->code,
                ':dName' => $this->name,
                ':cat' => date('Y-m-d h:m:s')
            )
        );

        if ($status) {
            $_SESSION['message'] = "Data Saved";
            header('location: ../department/save-department.php');
        }
        else
        {
            $_SESSION['message'] = "Error! Something Wrong !!";
            header('location: ../department/save-department.php?status=error');
        }
    }

    public function update()
    {
        $stmt = $this->connect()->prepare('UPDATE departments SET dept_code = :code, dept_name = :dName, updated_at = :uat WHERE id = :id');

        $status = $stmt->execute(
            array(
                ':id' => $this->id,
                ':code' => $this->code,
                ':dName' => $this->name,
                ':uat' => date('Y-m-d h:m:s')
            )
        );

        if ($status) {
            $_SESSION['message'] = "Data Updated !!";
            header('location: ../department/departments.php');
        }
        else
        {
            $_SESSION['message'] = "Error! Something Wrong !!";
            header('location: ../department/departments.php?status=error');
        }
    }

    public function validation()
    {
        $allDept = $this->getAll('SELECT * FROM `departments`');
        $aDept = $this->getOne("SELECT * FROM `departments` WHERE id = $this->id");

        if (strlen($this->code) > 7 || strlen($this->code) < 2) //Code
        {
            $_SESSION['message'] = "Code must be 2 to 7 characters !!";
            if (empty($this->id))
            {
                header('location: ../department/save-department.php?status=error');
            }
            else
            {
                header('location: ../department/edit-department.php?status=error&id='.$this->id);
            }
            die();
        }
        else
        {
            if (empty($this->id))
            {
                foreach ($allDept as $value)
                {
                    if ($value['dept_code'] == $this->code && $value['is_delete'] != 1)
                    {
                        $_SESSION['message'] = "This code already exist !!";
                        header('location: ../department/save-department.php?status=error');
                        die();
                    }
                }
            }
        }



        if (empty($this->name)) //Name
        {
            $_SESSION['message'] = "You mast enter a department name !!";
            if (empty($this->id))
            {
                header('location: ../department/save-department.php?status=error');
            }
            else
            {
                header('location: ../department/edit-department.php?status=error&id='.$this->id);
            }
            die();
        }
        else
        {
            if (empty($this->id))
            {
                foreach ($allDept as $value)
                {
                    if ($value['dept_name'] == $this->name && $value['is_delete'] != 1)
                    {

                        $_SESSION['message'] = "This name already exist !!";
                        header('location: ../department/save-department.php?status=error');
                        die();

                    }
                }
            }
        }


        if (!empty($this->id)) //Duplicate validation in edit
        {
            if ($aDept['dept_code'] == $this->code && $aDept['dept_name'] == $this->name)
            {
                $_SESSION['message'] = "You can't submit same data !!";
                header('location: ../department/edit-department.php?status=error&id='.$this->id);
                die();
            }

            foreach ($allDept as $value)
            {
                if ($value['dept_code'] == $this->code)
                {
                    if ($aDept['dept_code'] != $value['dept_code'])
                    {
                        $_SESSION['message'] = "This code already exist !!";
                        header('location: ../department/edit-department.php?status=error&id=' . $this->id);
                        die();
                    }
                }
            }

            foreach ($allDept as $value)
            {
                if ($value['dept_name'] == $this->name)
                {
                    if ($aDept['dept_name'] != $value['dept_name'])
                    {
                        $_SESSION['message'] = "This name already exist !!";
                        header('location: ../department/edit-department.php?status=error&id=' . $this->id);
                        die();
                    }

                }
            }
        }

        unset($_SESSION['formData']);
        return $this;
    }
}