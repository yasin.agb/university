<?php

namespace App\Database;

use PDO;

class Database
{
    private $host = 'localhost';
    private $name = 'university';
    private $userName = 'root';
    private $password = '';

    public function connect()
    {
        $objPDO = new PDO('mysql:host='.$this->host.';dbname='.$this->name.'', ''.$this->userName.'', ''.$this->password.'');
        return $objPDO;
    }

    public function getOne($query = '')
    {
        $stmt = $this->connect()->prepare($query);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getAll($query = '')
    {
        $stmt = $this->connect()->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function delete($id, $tableName)
    {
        $stmt = $this->connect()->prepare("UPDATE `$tableName` SET `is_delete`=:isDelete, `deleted_at`=:dat WHERE id=:id");
        $status = $stmt->execute(
            array(
                ':isDelete' => 1,
                ':id' => $id,
                ':dat' => date('Y-m-d h:m:s')
            )
        );
        return $status;
    }

    public function undo($id, $tableName)
    {
        $stmt = $this->connect()->prepare("UPDATE `$tableName` SET `is_delete`=:isDelete, `deleted_at`=:dat WHERE id=:id");
        $status = $stmt->execute(
            array(
                ':isDelete' => 0,
                ':id' => $id,
                ':dat' => '0000-00-00 00:00:00'
            )
        );
        return $status;
    }

    public function deleteAll($tableName)
    {
        $stmt = $this->connect()->prepare("UPDATE `$tableName` SET `is_delete`=:isDelete, `deleted_at`=:dat");
        $status = $stmt->execute(
            array(
                ':isDelete' => 1,
                ':dat' => date('Y-m-d h:m:s')
            )
        );
        return $status;
    }

    public function undoAll($tableName)
    {
        $stmt = $this->connect()->prepare("UPDATE `$tableName` SET `is_delete`=:isDelete, `deleted_at`=:dat");
        $status = $stmt->execute(
            array(
                ':isDelete' => 0,
                ':dat' => '0000-00-00 00:00:00'
            )
        );
        return $status;
    }
}