<?php

namespace App\Course;

use App\Database\Database;
use App\Utility\Utility;

class Course extends Database
{
    private $id;
    private $code;
    private $name;
    private $credit;
    private $description;
    private $department;
    private $semester;

    public function setData($data = "")
    {
        if(!empty($data['code'])) {
            $this->code = strtoupper($data['code']);

        }
        if(!empty($data['name'])) {
            $this->name = ucwords(strtolower($data['name']));
        }
        if(!empty($data['id'])) {
            $this->id = $data['id'];
        }
        if(!empty($data['credit'])) {
            $this->credit = $data['credit'];
        }
        if(!empty($data['description'])) {
            $this->description = $data['description'];
        }
        if(!empty($data['department'])) {
            $this->department = $data['department'];
        }
        if(!empty($data['semester'])) {
            $this->semester = $data['semester'];
        }
        return $this;
    }

    public function store()
    {
        $stmt = $this->connect()->prepare('INSERT INTO courses(course_code, course_name, credit, description, dept_id, semester_id, created_at)
                                                    VALUES(:code, :cName, :credit, :description, :did, :sid, :cat)');

        $status = $stmt->execute(
        array(
            ':code' => $this->code,
            ':cName' => $this->name,
            ':credit' => $this->credit,
            ':description' => $this->description,
            ':did' => $this->department,
            ':sid' => $this->semester,
            ':cat' => date('Y-m-d h:m:s')
        )
        );

        if ($status) {
            $_SESSION['message'] = "Data Saved";
            header('location: ../course/save-course.php');
        }
        else
        {
            $_SESSION['message'] = "Error! Something Wrong !!";
            header('location: ../course/save-course.php?status=error');
        }
    }

    public function validation()
    {
        $allCourse = $this->getAll('SELECT * FROM `courses`');

        if (empty($this->code)) //Code
        {
            $_SESSION['message'] = "Code can't be empty !!";
            header('location: ../course/save-course.php?status=error ');
            die();
        }
        elseif (strlen($this->code) < 5)
        {
            $_SESSION['message'] = "Code minimum length 5 !!";
            header('location: ../course/save-course.php?status=error ');
            die();
        }
        else
        {
            foreach ($allCourse as $value)
            {
                if ($value['course_code'] == $this->code) {
                    $_SESSION['message'] = "This code already exist !!";
                    header('location: ../course/save-course.php?status=error ');
                    die();
                }
            }
        }


        if (empty($this->name)) //Name
        {
            $_SESSION['message'] = "Name can't be empty !!";
            header('location: ../course/save-course.php?status=error ');
            die();
        }
        else
        {
            foreach ($allCourse as $value)
            {
                if ($value['course_name'] == $this->name) {
                    $_SESSION['message'] = "This name already exist !!";
                    header('location: ../course/save-course.php?status=error ');
                    die();
                }
            }
        }


        if (empty($this->credit)) //Credit
        {
            $_SESSION['message'] = "Credit can't be empty !!";
            header('location: ../course/save-course.php?status=error ');
            die();
        }
        elseif (!filter_var($this->credit, FILTER_VALIDATE_FLOAT))
        {
            $_SESSION['message'] = "Credit must digit only !!";
            header('location: ../course/save-course.php?status=error ');
            die();
        }
        elseif ( 0.5 > $this->credit || 5 < $this->credit)
        {
            $_SESSION['message'] = "Credit range 0.5 to 5.0 !!";
            header('location: ../course/save-course.php?status=error ');
            die();
        }

        if (empty($this->description)) //description
        {
            $this->description = '';
        }
        else
        {
            $this->description = htmlspecialchars($this->description);
        }


        if (empty($this->department)) //department
        {
            $_SESSION['message'] = "Department can't be empty !!";
            header('location: ../course/save-course.php?status=error ');
            die();
        }

        if (empty($this->semester)) //semester
        {
            $_SESSION['message'] = "Semester can't be empty !!";
            header('location: ../course/save-course.php?status=error ');
            die();
        }

        unset($_SESSION['formData']);
        return $this;
    }
}